package com.db.graduatetraining.analyzer.webapp;


import com.db.graduatetraining.analyzer.midtier.domain.AveragePrice;
import com.db.graduatetraining.analyzer.midtier.AveragePriceService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;

@Path("/averagePrice")
public class AveragePriceController {
	final private AveragePrice AveragePrice = new AveragePrice();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<AveragePrice> getAll() throws SQLException {
		return AveragePriceService.getAll();
	}
}
