package com.db.graduatetraining.analyzer.webapp;

import com.db.graduatetraining.analyzer.midtier.dao.CounterpartyDao;
import com.db.graduatetraining.analyzer.midtier.domain.DomainObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;

@Path("/counterparties")
public class CounterpartyController {
	final private CounterpartyDao counterpartyDao = new CounterpartyDao();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<? extends DomainObject> getAll(@QueryParam("with") String with) throws SQLException {
		if ("count".equals(with)) {
			return counterpartyDao.getAllWithCount();
		}
		return counterpartyDao.getAll();
	}
}
