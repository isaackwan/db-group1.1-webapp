package com.db.graduatetraining.analyzer.webapp;


import com.db.graduatetraining.analyzer.midtier.CounterpartyInstrumentCountService;
import com.db.graduatetraining.analyzer.midtier.domain.CounterpartyInstrumentCount;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;

@Path("/counterpartyInstrumentCounts")
public class CounterpartyInstrumentCountController {
	final private CounterpartyInstrumentCountService service = new CounterpartyInstrumentCountService();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<CounterpartyInstrumentCount> getAll() throws SQLException {
		return service.getAll();
	}
}
