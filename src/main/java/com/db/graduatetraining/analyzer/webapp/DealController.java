package com.db.graduatetraining.analyzer.webapp;

import com.db.graduatetraining.analyzer.midtier.dao.DealDao;
import com.db.graduatetraining.analyzer.midtier.domain.Deal;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;

@Path("/deals")
public class DealController {
	final private DealDao dealDao = new DealDao();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<? extends Deal> getAll(@QueryParam("with") String with) throws SQLException {
		if ("instrument,counterparty".equals(with) || "counterparty,instrument".equals(with)) {
			return dealDao.getAllWithRelatedEntities();
		}
		return dealDao.getAll();
	}
}
