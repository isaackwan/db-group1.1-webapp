package com.db.graduatetraining.analyzer.webapp;

import com.db.graduatetraining.analyzer.midtier.domain.DealHashMap;
import com.db.graduatetraining.analyzer.midtier.DealHashMapService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;

@Path("/DealHashMap")
public class DealHashMapController {
	final private DealHashMapService dealHashMapService = new DealHashMapService();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<DealHashMap> getAll() throws SQLException {
		return dealHashMapService.getAll();
	}
}
