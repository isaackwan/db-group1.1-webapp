package com.db.graduatetraining.analyzer.webapp;

import com.db.graduatetraining.analyzer.midtier.domain.dealsDone;
import com.db.graduatetraining.analyzer.midtier.dealsDoneService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;

@Path("/dealsDone")
public class DealsDoneController {
	final private dealsDone AveragePrice = new dealsDone();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<dealsDone> getAll() throws SQLException {
		return dealsDoneService.getAll();
	}
}
