package com.db.graduatetraining.analyzer.webapp;


import com.db.graduatetraining.analyzer.midtier.domain.EffectiveProfit;
import com.db.graduatetraining.analyzer.midtier.EffectiveProfitService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;

@Path("/effectiveProfit")

public class EffectiveProfitController {
	final private EffectiveProfit effectiveProfit = new EffectiveProfit();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<EffectiveProfit> getAll() throws SQLException {
		return EffectiveProfitService.getAll();
	}
}
