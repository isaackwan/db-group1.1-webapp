package com.db.graduatetraining.analyzer.webapp;

import com.db.graduatetraining.analyzer.midtier.dao.InstrumentDao;
import com.db.graduatetraining.analyzer.midtier.domain.DomainObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;

@Path("/instruments")
public class InstrumentController {
	final private InstrumentDao instrumentDao = new InstrumentDao();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<? extends DomainObject> getAll(@QueryParam("with") String with) throws SQLException {
		if ("count".equals(with)) {
			return instrumentDao.getAllWithCount();
		}
		return instrumentDao.getAll();
	}
}
