package com.db.graduatetraining.analyzer.webapp;


import com.db.graduatetraining.analyzer.midtier.domain.realisedProfit;
import com.db.graduatetraining.analyzer.midtier.RealisedProfitService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;

@Path("/realisedProfit")

public class RealisedProfitController {
	final private realisedProfit AveragePrice = new realisedProfit();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<realisedProfit> getAll() throws SQLException {
		return RealisedProfitService.getAll();
	}
}
