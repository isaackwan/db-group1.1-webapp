package com.db.graduatetraining.analyzer.webapp;

import com.db.graduatetraining.analyzer.midtier.dao.UserDao;
import com.db.graduatetraining.analyzer.midtier.domain.User;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

@Path("/users")
public class UserController {
	final private UserDao userDao = new UserDao();

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<User> getAll() throws SQLException {
		return userDao.getAll();
	}
}
