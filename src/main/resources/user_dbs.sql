CREATE DATABASE db_grad CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
USE db_grad;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `username` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(400) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
INSERT INTO `users` VALUES ('isaac','028677007a00c541472ed0f0fde525e5443f044a0afb5c331d70017a0ec40b4e$1$df9f79b95c2f4a6401e2cfeb03103cc365c6fd8f');

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `username` varchar(15) COLLATE utf8mb4_bin NOT NULL,
  `role_name` varchar(15) COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`username`,`role_name`),
  CONSTRAINT `user_roles_users_FK` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
INSERT INTO `user_roles` VALUES ('isaac','user');
