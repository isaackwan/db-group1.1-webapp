<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="theAccount" class="com.db.graduatetraining.analyzer.webapp.Account" scope="application"/>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Balance of the Account is <%=theAccount.credit(theAccount.getBalance() + 10 )%> </h1>
    </body>
</html>
