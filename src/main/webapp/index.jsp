<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Data Visualisation Application</title>
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css">
</head>
<body>

	<link rel="stylesheet" type="text/css" href="lib/css/main.css">
	<link rel="stylesheet" type="text/css" href="lib/css/task4v2.css">
	<link rel="stylesheet" type="text/css" href="lib/css/shen.css">

	<div id="task4v2-tooltip">
		<div class="text">Tooltip content goes here...</div>
		<div class="small">Click to <i>pin</i> the entity</div>
	</div>


<!-- CSS -->
<link rel="stylesheet" type="text/css" href="lib/css/main.css">

<div class="limiter">
	<div class="tableContainerBackground">
	<div id="loader" class="loader"></div>
	<div id="tableContainer" >
	<table class="simpleTable"></table>
	<iframe id="task4v1" src="task4v1.html"></iframe>
	<div class="task4v2-container"> <!-- For Isaac's old user requirements 1 -->
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-3">
					<div class="instruments sidebar"></div>
				</div>
				<div class="col-xs-6">
					<h4>
						Deals
					</h4>
					<div class="table-responsive">
						<table class="table table-striped table-hover table-condensed ">
							<thead>
								<tr>
									<td>ID</td>
									<td>Type</td>
									<td>Time</td>
									<td>Amount</td>
									<td>Total Price</td>
									<td>Quantity</td>
									<td>Instrument</td>
									<td>Counterparty</td>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="counterparties sidebar"></div>
				</div>
			</div>
		</div>
	</div>
	<div id="shenContainer"></div>
	<div id="navBar">

    	<nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <p class="brandHeader"><strong>DB</strong></p>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav ">

                <li id="simpleTableBtn" class="dropdown">
                <a  class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> <strong>Table Views<span class="caret"></span> </strong>
                </a>
                <ul class="dropdown-menu simpleMenu" id="simpleMenu">
                <li class="simpleItems"><a id="buttonInstruments"  href="#">Instruments</a></li>
                <li class="simpleItems"><a id="buttonDeal" href="#">Deals</a></li>
                <li class="simpleItems"><a id="buttonCounterparty" href="#">CounterParties</a></li>
                 <li class="simpleItems"><a id="buttonUsers" href="#">Users</a></li>
                 <li class="simpleItems"><a id="buttonDealsDone" href="#">Deals Done</a></li>
                 <li class="simpleItems"><a id="buttonSellBuy" href="#">Average prices</a></li>
                 <li class="simpleItems"><a id="buttonProfit" href="#">Realised profit</a></li>
				<li class="simpleItems"><a id="effectiveButtonProfit" href="#">Effective profit</a></li>
                </ul>
                </li>
                <li id="isaacsTableBtn" class="dropdown">
                <a href="#" role="button"> <strong>Relational View</strong>

                </a>
                </li>
                <li id="networkViewButton" class="dropdown">
				<a href="#" role="button"> <strong>Network View</strong>

				</a>
				</li>








<ul class="pagination" id="paginationContainer"></ul>

<li id="inputLi" class="simpleItems"><div class="input-group"><input class="form-control" aria-describedby="searchIcon" type="text" id="searchInput" placeholder="Search here..">
</div></li>

<li id="pageLi" class="simpleItems"><div class="input-group"><input class="form-control" aria-describedby="searchIcon" type="text" id="pageInput" placeholder="Page # of 100">

</div></li>

              </ul>



<!--
<span class="input-group-addon" id="searchIcon" class="glyphicon glyphicon-search" aria-hidden="true"></span>
-->
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

    	</div>
	</div>



	</div>
	</div>
</div>

		<div class="alert alert-warning alert-dismissible " role="alert" id="progressDialog">
			<strong >You logged in!</strong>
			<p id="progresstxtName"></p>
			<p id="progresstxtPassword"></p>
			<p id="closing">Closing in: 11</p>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times</span>
			</button>
		</div>

	<!-- Main Javascript-->
	<script src="lib/bootstrap/js/jquery-3.3.1.min.js">
	</script>
	<script src="lib/bootstrap/js/bootstrap.min.js">
	</script>
	<script src="lib/d3/d3.min.js">
	</script>
	<script src="lib/d3/d3_3.js">
	</script>
	<script src="lib/d3/d3.v4.min.js">
	</script>
	<script src="lib/js/main.js">
	</script>
	<script src="lib/js/task4v2.js">
	</script>
	<script src="lib/js/task4v1.js">
	</script>
		<script src="lib/js/dealsDone.js">
    	</script>
</body>
</html>