$(function(){
	var dealsDoneData;
	var instrumentNameArray;


$("#buttonDealsDone").on("click",function () {
				$("#loader").fadeIn("fast");
    			hookBeforeSwitchingSimpleTables(function(){
					$("#shenContainer").show(0);
					$("#loader").fadeOut("fast");

  $.getJSON("api/DealHashMap", function (data,error) {
  					dealsDoneData=[];
  					instrumentNameArray=[];

				 for (var key in data) {
                        dealsDoneData.push({"counterpartyName": data[key].counterpartyName, "instrumentDeal":[]} );
                        }
                 for(let y=0;y<dealsDoneData.length;y++){
                           var i =0;
                 for ( var key in data[y].instrumentDeal){
                          dealsDoneData[y].instrumentDeal[i]={"value": data[y].instrumentDeal[key] ,"instrumentName":key };
                             i++
                               }
                 			}
                          data.forEach(function(element){
                                instrumentNameArray.push(element.counterpartyName);
                          })

                                  		$("#shenContainer").append("<p id=dealDoneFont>Please Select Users : </p>");

                                  		instrumentNameArray.forEach(function(element){
                              			$('#shenContainer').append("<button class=userSelection value= "+ element + ">" + element + "</button>");
                              			$("#loader").fadeOut("fast");
                                     		});
                              			$('#shenContainer').append("<div id=dealsDoneChart></div>");
                });


        		//

    			});
    		///


    		});



$(document).on("click", ".userSelection", function(){
        $('#dealsDoneChart').children().remove();
        index = dealsDoneData.findIndex(x => x.counterpartyName== $(this).attr("value"));
        $("#dealDoneFont").html("Number of deals done by  <span style='color:red'>" + $(this).attr("value") +"</span>" );
        tabulatingPieChart(dealsDoneData[index].instrumentDeal);
    });


function tabulatingPieChart(dataset) {// chart dimensions
var width = 1053;
var height = 400;
// a circle chart needs a radius
var radius = 160;
console.log(radius);

// legend dimensions
var legendRectSize = 20; // defines the size of the colored squares in legend
var legendSpacing = 6; // defines spacing between squares

// define color scale
var color = d3v4.scaleOrdinal(d3v4.schemeCategory20b);
//var color = randomColor;
// more color scales: https://bl.ocks.org/pstuffa/3393ff2711a53975040077b7453781a9

var svg = d3v4.select('#dealsDoneChart') // select element in the DOM with id 'chart'
.append('svg') // append an svg element to the element we've selected
.attr('width', width) // set the width of the svg element we just added
.attr('height', height) // set the height of the svg element we just added
.append('g') // append 'g' element to the svg element
.attr('transform', 'translate(' + ((width/ 2)-150) + ',' + ((height / 2)-30) + ')'); // our reference is now to the 'g' element. centerting the 'g' element to the svg element

var arc = d3v4.arc()
.innerRadius(0) // none for pie chart
.outerRadius(radius); // size of overall chart

var pie = d3v4.pie() // start and end angles of the segments
.value(function(d) {return d.value;}) // how to extract the numerical data from each entry in our dataset
.sort(null); // by default, data sorts in oescending value. this will mess with our animation so we set it to null

// define tooltip
var tooltip = d3v4.select('#dealsDoneChart') // select element in the DOM with id 'chart'
.append('div') // append a div element to the element we've selected
.attr('class', 'shen-tooltip'); // add class 'tooltip' on the divs we just selected

tooltip.append('div') // add divs to the tooltip defined above
.attr('class', 'shenLabel'); // add class 'label' on the selection

tooltip.append('div') // add divs to the tooltip defined above
.attr('class', 'count'); // add class 'count' on the selection

tooltip.append('div') // add divs to the tooltip defined above
.attr('class', 'percent'); // add class 'percent' on the selection

// Confused? see below:

// <div id="chart">
//   <div class="tooltip">
//     <div class="label">
//     </div>
//     <div class="count">
//     </div>
//     <div class="percent">
//     </div>
//   </div>
// </div>

dataset.forEach(function(d) {
d.value = +d.value; // calculate count as we iterate through the data
d.enabled = true; // add enabled property to track which entries are checked
});

// creating the chart
var path = svg.selectAll('path') // select all path elements inside the svg. specifically the 'g' element. they don't exist yet but they will be created below
.data(pie(dataset)) //associate dataset wit he path elements we're about to create. must pass through the pie function. it magically knows how to extract values and bakes it into the pie
.enter() //creates placeholder nodes for each of the values
.append('path') // replace placeholders with path elements
.attr('d', arc) // define d attribute with arc function above
.attr('fill', function(d) {
return color(d.data.instrumentName);
}) // use color scale to define fill of each label in dataset
.each(function(d) {
this._current - d;
}); // creates a smooth animation for each track

// mouse event handlers are attached to path so they need to come after its definition
path.on('mouseover', function(d) {
// when mouse enters div
var total = d3v4.sum(dataset.map(function(d) {// calculate the total number of tickets in the dataset
return (d.enabled) ? d.value: 0; // checking to see if the entry is enabled. if it isn't, we return 0 and cause other percentages to increase
}));
var percent = Math.round(1000 * d.data.value / total) / 10; // calculate percent
tooltip.select('.shenLabel').html(d.data.instrumentName); // set current label
tooltip.select('.count').html("Count : " + d.data.value); // set current count
tooltip.select('.percent').html(percent + '%'); // set percent calculated above
tooltip.style('display', 'block'); // set display
});

path.on('mouseout', function() {// when mouse leaves div
tooltip.style('display', 'none'); // hide tooltip for that element
});

path.on('mousemove', function(d) {
// when mouse moves
tooltip.style('top', (d3v4.event.layerY + 10) + 'px') // always 10px below the cursor
.style('left', (d3v4.event.layerX + 10) + 'px'); // always 10px to the right of the mouse
});

// define legend
var legend = svg.selectAll('.legend') // selecting elements with class 'legend'
.data(color.domain()) // refers to an array of labels from our dataset
.enter() // creates placeholder
.append('g') // replace placeholders with g elements
.attr('class', 'legend') // each g is given a legend class
.attr('transform', function(d, i) {
var height = legendRectSize + legendSpacing; // height of element is the height of the colored square plus the spacing
var offset =  height * color.domain().length / 2; // vertical offset of the entire legend = height of a single element & half the total number of elements
var horz = 18 * legendRectSize; // the legend is shifted to the left to make room for the text
var vert = i * height - offset; // the top of the element is hifted up or down from the center using the offset defiend earlier and the index of the current element 'i'
return 'translate(' + horz + ',' + vert + ')'; //return translation
});

// adding colored squares to legend
legend.append('rect') // append rectangle squares to legend
.attr('width', legendRectSize) // width of rect size is defined above
.attr('height', legendRectSize) // height of rect size is defined above
.style('fill', color) // each fill is passed a color
.style('stroke', color); // each stroke is passed a color


// adding text to legend
legend.append('text')
.attr('id', "legendText")
.attr('x', legendRectSize + legendSpacing)
.attr('y', legendRectSize - legendSpacing)
.text(function(d) {
return d;
});
};

});