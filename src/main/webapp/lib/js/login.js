
(function ($) {
 username=" ";
 password=" ";
    "use strict";
 	/* Main Logic */

$(document).on("submit", "#loginForm", function(e) {
			e.preventDefault();
			if ($('.validate-form').on('submit',function(){
                    var check = true;

                    for(var i=0; i<input.length; i++) {

                        if(validate(input[i]) == false){
                            showValidate(input[i]);
                            check=false;
                        }
                    }
                    return check;
                }) === false){return;}
            else {
             	 $.ajax({
              				url: $(this).attr('action'),
              				type: $(this).attr('method'),
              				data: $(this).serialize(),
              				success: function(data) {
              				console.log("Authentication results", data);
              				if (typeof data === "object" && data.message) {
              					alert(data.message);
              				} else if (typeof data === "string") {
              					//If String type hence login valid
              					validateLogin();
              					setTimeout(function replaceBody() {
              						$("#ajax-container").replaceWith(data);
              					}, 800);
              				} else {
              					console.error("Unknown data returned, refreshing...");
              					// window.location.reload(true);
              				}
              				},
              				error: function(xhr, err) {
              					alert("There was an error while submitting");
              					console.error("Error with XHR", xhr, err);
              				},
              			});



              };


});



 function validateLogin(){
     $("#dblogo").fadeOut("fast");
 	 $(".login100-form").fadeOut("fast");
 	 $("#loader").fadeIn("fast");
  	 $("#loginContainer").hide(700,"swing", function(){
   	 $("#loginContainer").detach();
 $("#progressDialog").fadeIn("slow");
      });

    }

/*==================================================================
    [ Validating if User input null ]*/
    var input = $('.validate-input .input100');
    $('.validate-form').on('submit',function(){
        var check = true;
		if(input.length>0){
		username=input[0].value;
		password=input[1].value;
		}
        for(var i=0; i<input.length; i++) {

            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    // Return false if Input is NULL
    function validateNullInput(){
    	let check = true ;
    	for(let i=0; i<input.length; i++) {
                    if(validate(input[i]) == false){
                        showValidate(input[i]);
                        check=false;
                    }
                }
         return check;
    }



    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }
    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }


})(jQuery);
