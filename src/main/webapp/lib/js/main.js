var hookBeforeSwitchingSimpleTables;

$(function() {
	var tableData;
	var colus;
	if (typeof username !== 'undefined') {
		var lefttime = 10;
		$("#progresstxtName").html("username: " + username)
		$("#progresstxtPassword").html("password: XXXXXXXXXX");

		var closingInter = setInterval(function() {
			$("#closing").html("Closing in " + lefttime);
			lefttime--;
			if (lefttime < 0) {
				clearInterval(closingInter);
				$("#progressDialog").fadeOut("fast");
			}
		}, 1000);

	} else {
		$("#progressDialog").fadeOut("fast");
	}
	$('.dropdown-toggle').dropdown();
	/*
	now handled in task4v2.js

	$("#isaacsTableBtn").on("click", function() {
		$(".simpleItems").css("visibility", "hidden");

		$("#simpleTableBtn").removeClass("active");
		$("#isaacsTableBtn").addClass("active");
	})*/
	$("#simpleTableBtn").on("click", function() {
		$(".simpleItems").css("visibility", "visible");
		$("#simpleTableBtn").addClass("active");
		$("#isaacsTableBtn").removeClass("active");
		$("#networkViewButton").removeClass("active");
	})
	$("#tableContainer").show(1000, "swing", function() {
		$("#tableContainer").css("display", "flex")
	});
	$("#loader").fadeOut("fast");
	$("#buttonInstruments").on("click", function() {
		hookBeforeSwitchingSimpleTables(function() {
			tabulationSimple("api/instruments");
		});
	});
	$("#buttonDeal").on("click", function() {
		hookBeforeSwitchingSimpleTables(function() {
			tabulationSimple("api/deals");
		});
	});
	$("#buttonCounterparty").on("click", function() {
		hookBeforeSwitchingSimpleTables(function() {
			tabulationSimple("api/counterparties");
		});
	});

	$("#buttonUsers").on("click", function() {
		hookBeforeSwitchingSimpleTables(function() {
			tabulationSimple("api/users",
				function() {});
		});
	});

	$("#buttonSellBuy").on("click", function() {
		$("#tableContainer").css("min-height", $("#tableContainer").css("height"));
		hookBeforeSwitchingSimpleTables(function() {
			$("#tableContainer table.simpleTable").hide("fast", function() {
				$("#loader").fadeIn("fast");
				averageSellBuyTable();
			});
		})

	});


	$("#buttonProfit").on("click", function() {
		$("#tableContainer").css("min-height", $("#tableContainer").css("height"));
		hookBeforeSwitchingSimpleTables(function() {
			$("#tableContainer table.simpleTable").hide("fast", function() {
				$("#loader").fadeIn("fast");
				populateProfitTable("realised");
			});
		})

	});
	$("#effectiveButtonProfit").on("click", function() {
		$("#tableContainer").css("min-height", $("#tableContainer").css("height"));
		hookBeforeSwitchingSimpleTables(function() {
			$("#tableContainer table.simpleTable").hide("fast", function() {
				$("#loader").fadeIn("fast");
				populateProfitTable("effective");
			})
		});


	});

	hookBeforeSwitchingSimpleTables = function(cb) {
		$("iframe#task4v1, #tableContainer .task4v2-container, #tableContainer table.simpleTable, svg.maxSvg, #shenContainer,svg#dianaChart,svg#realisedProfitChart").fadeOut(200).promise().done(function() {
			$("svg.maxSvg").remove();
			$('#shenContainer').empty();
			$(".task4v2-container tbody, .task4v2-container .instruments, .task4v2-container .counterparties").empty();
			cb();
		});
	}

	function tabulationSimple(url) {
		$("#tableContainer").css("min-height", $("#tableContainer").css("height"));
		$("#tableContainer table.simpleTable").hide("fast", function() {
			$("#tableContainer table.simpleTable").remove();
			$("#loader").fadeIn("fast");
			//$("#tableContainer").children().remove();
			$.getJSON(url, function(data, error) {
				function tabulate(data, columns) {
					$("#loader").fadeOut("fast");
					var sortAscending = true;
					tableData = data;
					colus = columns;
					console.log(tableData.length % 100);
					if (tableData.length > 99) {

						showTablePage(1, columns)
						$("#pageInput").attr("placeholder", "Page # of " + Math.round(tableData.length / 99));

						$("#pageLi").css("display", "block");
					} else {
						$("#pageLi").css("display", "none");


						var table = d3.select('#tableContainer').append('table')
							//	.attr("id","simpleTableLayout");
							.attr("class", "table table-hover simpleTable")
							.attr("id", "tabl");

						var thead = table.append('thead')

						console.log(data)
						var tbody = table.append('tbody');

						// append the header row
						thead.append('tr')
							.selectAll('th')
							.data(columns).enter()
							.append('th')
							.text(function(column) {
								return column;
							})
							.on('click', function(d) {
								thead.attr('class', 'header')
								if (sortAscending) {
									rows.sort(function(a, b) {
										if (typeof(a[d]) === "number") {
											return a[d] - b[d];
										} else {
											return b[d].toLowerCase() < a[d].toLowerCase();
										}
									});
									sortAscending = false;
									// this.className = 'aes';
								} else {
									rows.sort(function(a, b) {
										if (typeof(a[d]) === "number") {
											return b[d] - a[d];
										} else {
											return b[d].toLowerCase() > a[d].toLowerCase();
										}
									});

									sortAscending = true;
									//	 this.className = 'des';
								}
							});

						// create a row for each object in the data
						var rows = tbody.selectAll('tr')
							.data(data)
							.enter()
							.append('tr');

						// create a cell in each row for each column
						var cells = rows.selectAll('td')

							.data(function(row) {
								return columns.map(function(column) {
									return {
										column: column,
										value: row[column]
									};
								});
							})
							.enter()
							.append('td')
							.attr("class", "tditem")
							.text(function(d) {
								return d.value;
							});
						$("#tableContainer").css("min-height", "");
						$("#tabl").show("fast").css("display", "slow");


						// $("#tabl").css("visibility","visible");
						return table;
					}
				}
				// render the table(s)
				let columnsArray = [];
				for (let i in data[0]) {
					columnsArray.push(i);
				}
				//console.log(columnsArray);
				tabulate(data, columnsArray, function() {});
			});
		});
	}

	function showTablePage(pageNr, cols) {
		$("#tableContainer table.simpleTable").remove();
		var data2 = [];
		for (var i = 0; i < 100; i++) {
			data2.push(tableData[pageNr * 100 + i]);
		}
		var table = d3.select('#tableContainer').append('table')
			//	.attr("id","simpleTableLayout");
			.attr("class", "table table-hover simpleTable")
			.attr("id", "tabl");

		var thead = table.append('thead');


		var tbody = table.append('tbody');

		// append the header row
		thead.append('tr')
			.selectAll('th')
			.data(cols).enter()
			.append('th')
			.text(function(column) {
				return column;
			})
			.on('click', function(d) {
				thead.attr('class', 'header')
				if (sortAscending) {
					rows.sort(function(a, b) {
						if (typeof(a[d]) === "number") {
							return a[d] - b[d];
						} else {
							return b[d].toLowerCase() < a[d].toLowerCase();
						}
					});
					sortAscending = false;
					// this.className = 'aes';
				} else {
					rows.sort(function(a, b) {
						if (typeof(a[d]) === "number") {
							return b[d] - a[d];
						} else {
							return b[d].toLowerCase() > a[d].toLowerCase();
						}
					});

					sortAscending = true;
					//	 this.className = 'des';
				}
			});



		// create a row for each object in the data
		var rows = tbody.selectAll('tr')
			.data(data2)
			.enter()
			.append('tr');

		// create a cell in each row for each column
		var cells = rows.selectAll('td')

			.data(function(row) {
				return cols.map(function(column) {
					return {
						column: column,
						value: row[column]
					};
				});
			})
			.enter()
			.append('td')
			.attr("class", "tditem")
			.text(function(d) {
				return d.value;
			});
		$("#tableContainer").css("min-height", "");
		$("#tabl").show("fast").css("display", "slow");


		// $("#tabl").css("visibility","visible");
		return table;
	}
	$("#pageInput").on('keyup', function(e) {
		if (e.keyCode == 13) {
			if ($("#pageInput").val() != "" && $.isNumeric($("#pageInput").val())) {

				showTablePage($("#pageInput").val(), colus);

			}
		}
	});

	$("#searchInput").on("keyup", function() {
		// Declare variables
		var input, filter, table, tr, td, i;
		input = document.getElementById("searchInput");
		filter = input.value.toUpperCase();
		table = document.getElementById("tabl");
		if (table != undefined) {
			tr = table.getElementsByTagName("tr");

			// Loop through all table rows, and hide those who don't match the search query
			if (tr.length > 0) {
				for (i = 1; i < tr.length; i++) {
					rowContains = false;
					tds = tr[i].getElementsByTagName("td");
					for (var j = 0; j < tds.length; j++) {
						td = tds[j];
						if (td) {
							if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
								//tr[i].style.display = "";
								rowContains = true;
							} else {
								//tr[i].style.display = "none";
							}
						}
					}
					if (!rowContains) {
						tr[i].style.display = "none";
					} else {
						tr[i].style.display = "";
					}
				}
			}
		}
	})

	function populateProfitTable(kind) {
		console.log(kind);

		$("svg").remove();
		var margin = {
				top: 20,
				right: 30,
				bottom: 40,
				left: 30
			},
			width = window.innerWidth * .8 - margin.left - margin.right,
			height = window.innerHeight * .7 - margin.top - margin.bottom;

		var x = d3.scale.linear()
			.range([0, width]);

		var y = d3.scale.ordinal()
			.rangeRoundBands([0, height], 0.1);

		var xAxis = d3.svg.axis()
			.scale(x)
			.orient("bottom");

		var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.tickSize(0)
			.tickPadding(6);

		var svg = d3.select("#tableContainer").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.attr("id", "realisedProfitChart")
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		if (kind == "realised") {
			d3.json("api/realisedProfit", function(error, data) {
				if (error) {
					console.error("Error in main.js d3", error)
				}
				x.domain(d3.extent(data, function(d) {
					return d.profit;
				})).nice();
				y.domain(data.map(function(d) {
					return d.counterparty_name;
				}));

				svg.selectAll(".bar")
					.data(data)
					.enter().append("rect")
					.attr("class", function(d) {
						return "bar bar--" + (d.profit < 0 ? "negative" : "positive");
					})
					.attr("x", function(d) {
						return x(Math.min(0, d.profit));
					})
					.attr("y", function(d) {
						return y(d.counterparty_name);
					})
					.attr("width", function(d) {
						return Math.abs(x(d.profit) - x(0));
					})
					.attr("height", y.rangeBand());

				svg.append("g")
					.attr("class", "x axis")
					.attr("transform", "translate(0," + height + ")")
					.call(xAxis);

				svg.append("g")
					.attr("class", "y axis")
					.attr("transform", "translate(" + x(0) + ",0)")
					.call(yAxis);
				$("#tableContainer").css("min-height", "");
				$("#loader").fadeOut("fast");

			});
		} else if (kind == "effective") {
			d3.json("api/effectiveProfit", function(error, data) {
				if (error) {
					console.error("Error in main.js d3", error)
				}
				x.domain(d3.extent(data, function(d) {
					return d.effectiveProfit;
				})).nice();
				y.domain(data.map(function(d) {
					return d.counterparty_name;
				}));

				svg.selectAll(".bar")
					.data(data)
					.enter().append("rect")
					.attr("class", function(d) {
						return "bar bar--" + (d.effectiveProfit < 0 ? "negative" : "positive");
					})
					.attr("x", function(d) {
						return x(Math.min(0, d.effectiveProfit));
					})
					.attr("y", function(d) {
						return y(d.counterparty_name);
					})
					.attr("width", function(d) {
						return Math.abs(x(d.effectiveProfit) - x(0));
					})
					.attr("height", y.rangeBand());

				svg.append("g")
					.attr("class", "x axis")
					.attr("transform", "translate(0," + height + ")")
					.call(xAxis);

				svg.append("g")
					.attr("class", "y axis")
					.attr("transform", "translate(" + x(0) + ",0)")
					.call(yAxis);
				$("#tableContainer").css("min-height", "");
				$("#loader").fadeOut("fast");

			});
		}

		function type(d) {
			d.profit = +d.profit;
			return d;
		}
	}

	function averageSellBuyTable() {

		$("svg").remove();

		var margin = {
				top: 20,
				right: 30,
				bottom: 40,
				left: 50
			},
			width = window.innerWidth * .8 - margin.left - margin.right,
			height = window.innerHeight * .7 - margin.top - margin.bottom;

		var x0 = d3.scale.ordinal()
			.rangeRoundBands([0, width], .1);

		var x1 = d3.scale.ordinal();

		var y = d3.scale.linear()
			.range([height, 0]);

		var xAxis = d3.svg.axis()
			.scale(x0)
			.tickSize(0)
			.orient("bottom");

		var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left");

		var color = d3.scale.ordinal()
			.range(["#ff5733", "#92c5de"]);

		var svg = d3.select("#tableContainer").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.attr("id", "dianaChart")
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		d3.json("api/averagePrice", function(error, data1) {
			var data = [];
			for (k in data1) {
				var z = {
					categorie: data1[k].name,
					values: [{
						value: data1[k].averageBuyPrice,
						rate: "Average Buy Price"
					}, {
						value: data1[k].averageSellPrice,
						rate: "Average Sell Price"
					}]
				};
				data.push(z);
			}

			var categoriesNames = data.map(function(d) {
				return d.categorie;
			});
			var rateNames = data[0].values.map(function(d) {
				return d.rate;
			});

			x0.domain(categoriesNames);
			x1.domain(rateNames).rangeRoundBands([0, x0.rangeBand()]);
			y.domain([0, d3.max(data, function(categorie) {
				return d3.max(categorie.values, function(d) {
					return d.value;
				});
			})]);

			svg.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis);

			svg.append("g")
				.attr("class", "y axis")
				.style('opacity', '0')
				.call(yAxis)
				.append("text")
				.attr("transform", "rotate(-90)")
				.attr("y", 6)
				.attr("dy", ".71em")
				.style("text-anchor", "end")
				.style('font-weight', 'bold')
				.text("Price");

			svg.select('.y').transition().duration(500).delay(1300).style('opacity', '1');

			var slice = svg.selectAll(".slice")
				.data(data)
				.enter().append("g")
				.attr("class", "g")
				.attr("transform", function(d) {
					return "translate(" + x0(d.categorie) + ",0)";
				});

			slice.selectAll("rect")
				.data(function(d) {
					return d.values;
				})
				.enter().append("rect")
				.attr("width", x1.rangeBand())
				.attr("x", function(d) {
					return x1(d.rate);
				})
				.style("fill", function(d) {
					return color(d.rate)
				})
				.attr("y", function(d) {
					return y(0);
				})
				.attr("height", function(d) {
					return height - y(0);
				})
				.on("mouseover", function(d) {
					d3.select(this).style("fill", d3.rgb(color(d.rate)).darker(2));
				})
				.on("mouseout", function(d) {
					d3.select(this).style("fill", color(d.rate));
				})
				.append("title").text(function(d) {
					return d.value.toFixed(2);
				});

			slice.selectAll("rect")
				.transition()
				.delay(function(d) {
					return Math.random() * 1000;
				})
				.duration(1000)
				.attr("y", function(d) {
					return y(d.value);
				})
				.attr("height", function(d) {
					return height - y(d.value);
				});

			//Legend
			var legend = svg.selectAll(".legend")
				.data(data[0].values.map(function(d) {
					return d.rate;
				}).reverse())
				.enter().append("g")
				.attr("class", "legend")
				.attr("transform", function(d, i) {
					return "translate(0," + i * 20 + ")";
				})
				.style("opacity", "0");

			legend.append("rect")
				.attr("x", width - 18)
				.attr("width", 18)
				.attr("height", 18)
				.style("fill", function(d) {
					return color(d);
				});

			legend.append("text")
				.attr("x", width - 24)
				.attr("y", 9)
				.attr("dy", ".35em")
				.style("text-anchor", "end")
				.text(function(d) {
					return d;
				});

			legend.transition().duration(500).delay(function(d, i) {
				return 1300 + 100 * i;
			}).style("opacity", "1");

		});
		$("#tableContainer").css("min-height", "");
		$("#loader").fadeOut("fast");
	}


});