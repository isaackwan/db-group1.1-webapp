$(function() {
	$("#networkViewButton a").click(function() {
		$("#simpleTableBtn").removeClass("active");
		$("#isaacsTableBtn").removeClass("active");
		$("#networkViewButton").addClass("active");
		hookBeforeSwitchingSimpleTables(function() {
			$("iframe#task4v1").show(0);
		});
	});
});