$(function() {

	// event: button - Isaac's table
	$("#isaacsTableBtn a").on("click", function (){
		// $(".simpleItems").css("visibility","hidden");
		$("#simpleTableBtn").removeClass("active");
		$("#networkViewButton").removeClass("active");
		$("#isaacsTableBtn").addClass("active");
		$("#loader").fadeIn("fast");
		hookBeforeSwitchingSimpleTables(function() {
			// $("#tableContainer table.simpleTable").remove();
			$("#tableContainer .task4v2-container").show(100, function() {
				toolbarWidthResize();
				loadData();
			});
		});
	})

	// "globals" -- references to objects
	var tooltip = $("#task4v2-tooltip");
	var table = $(".task4v2-container tbody");
	var instruments_sidebar = $(".task4v2-container .instruments");
	var counterparties_sidebar = $(".task4v2-container .counterparties");

	// event: rest of Isaac's table
	$(".task4v2-container").on("mouseover", ".tooltip-hint", function(event) {
		tooltip.css("visibility", "visible");
	});

	$(".task4v2-container").on("mouseout", ".tooltip-hint", function(event) {
		tooltip.css("visibility", "hidden");
	});

	$(".task4v2-container").on("mousemove", ".tooltip-hint", function(event) {
		var link = $(event.target)
		var tr = link.closest("tr");

		if (link.hasClass("view-instrument-link")) {
			var instrument = instruments[tr.data("deal").instrumentId];

			tooltip.children(".text").text("Instrument #" + instrument.id + ": " + instrument.name);
		} else if (link.hasClass("view-counterparty-link")) {
			var counterparty = counterparties[tr.data("deal").counterpartyId];

			// tooltip.text("Counterparty #" + counterparty.id + ": " + counterparty.name + "\nCount: " + counterparty.count + "\nStatus: " + counterparty.status + "\nDate Registered: " + counterparty.dateRegistered);
			tooltip.children(".text").text("Counterparty #" + counterparty.id + ": " + counterparty.name);
		} else {
			console.error("Unknown link type");
		}
		tooltip.css("top", (event.pageY - 30) + "px")
			.css("left", (event.pageX + 20) + "px");
	});

	$(".task4v2-container").on("click", ".card-close", function(event) {
		var button = $(event.target);
		var card = button.closest(".panel");
		card.remove();
	});

	$(".task4v2-container").on("click", ".view-instrument-link", function(event) {
		var link = $(event.target)
		var tr = link.closest("tr");
		var instrument = instruments[tr.data("deal").instrumentId];

		var card = $('<div class="panel panel-default"> <div class="panel-heading clearfix"> <button type="button" class="card-close btn btn-primary btn-xs pull-right">X</button> <h3 class="panel-title">CS (Counterparty #1)</h3> </div> <div class="panel-body card-body"></div> </div>');
		card.find(".panel-title").text("Instrument #" + instrument.id + ": " + instrument.name);
		card.find(".card-body").text("Count: " + instrument.count);
		card.prependTo(instruments_sidebar);

		return false;
	});

	$(".task4v2-container").on("click", ".view-counterparty-link", function(event) {
		var link = $(event.target)
		var tr = link.closest("tr");
		var counterparty = counterparties[tr.data("deal").counterpartyId];

		var card = $('<div class="panel panel-default"> <div class="panel-heading clearfix"> <button type="button" class="card-close btn btn-primary btn-xs pull-right">X</button> <h3 class="panel-title">CS (Counterparty #1)</h3> </div> <div class="panel-body card-body"></div> </div>');
		card.find(".panel-title").text("Counterparty #" + counterparty.id + ": " + counterparty.name);
		card.find(".card-body").text("Count: " + counterparty.count + "\nStatus: " + counterparty.status + "\nDate Registered: " + counterparty.dateRegistered);
		card.prependTo(counterparties_sidebar);

		return false;
	});

	function toolbarWidthResize() {
		$(".sidebar").each(function() {
			$(this).width($(this).parent().width());
		});
	}

	toolbarWidthResize();
	$(window).resize(toolbarWidthResize);

	function loadData() {
		/*
		var deals = [{"id":20001,"counterpartyId":1,"instrumentId":1,"type":"S","amount":3405.29,"quantity":6,"time":"2018-07-30T05:58:20.915","instrumentName":"Astronomica","counterpartyName":"Lina","counterpartyStatus":"A","counterpartyDateRegistered":"2014-01-01T00:00:00.000"},{"id":20094,"counterpartyId":1,"instrumentId":1,"type":"B","amount":3326.13,"quantity":177,"time":"2018-07-30T05:58:24.797","instrumentName":"Astronomica","counterpartyName":"Lina","counterpartyStatus":"A","counterpartyDateRegistered":"2014-01-01T00:00:00.000"},{"id":20271,"counterpartyId":5,"instrumentId":9,"type":"S","amount":2440.96,"quantity":248,"time":"2018-07-30T06:02:52.785","instrumentName":"Koronis","counterpartyName":"Estelle","counterpartyStatus":"A","counterpartyDateRegistered":"2001-01-01T00:00:00.000"},{"id":20587,"counterpartyId":5,"instrumentId":10,"type":"B","amount":10270.51,"quantity":18,"time":"2018-07-30T06:04:12.674","instrumentName":"Eclipse","counterpartyName":"Estelle","counterpartyStatus":"A","counterpartyDateRegistered":"2001-01-01T00:00:00.000"},{"id":20574,"counterpartyId":2,"instrumentId":10,"type":"B","amount":10121.25,"quantity":530,"time":"2018-07-30T06:04:12.428","instrumentName":"Eclipse","counterpartyName":"Richard","counterpartyStatus":"A","counterpartyDateRegistered":"2003-01-01T00:00:00.000"},{"id":20604,"counterpartyId":2,"instrumentId":10,"type":"B","amount":10461.28,"quantity":1,"time":"2018-07-30T06:04:12.689","instrumentName":"Eclipse","counterpartyName":"Richard","counterpartyStatus":"A","counterpartyDateRegistered":"2003-01-01T00:00:00.000"},{"id":20725,"counterpartyId":2,"instrumentId":10,"type":"B","amount":10314.34,"quantity":87,"time":"2018-07-30T06:04:20.199","instrumentName":"Eclipse","counterpartyName":"Richard","counterpartyStatus":"A","counterpartyDateRegistered":"2003-01-01T00:00:00.000"},{"id":20767,"counterpartyId":2,"instrumentId":10,"type":"S","amount":10103.78,"quantity":37,"time":"2018-07-30T06:04:22.027","instrumentName":"Eclipse","counterpartyName":"Richard","counterpartyStatus":"A","counterpartyDateRegistered":"2003-01-01T00:00:00.000"},{"id":20781,"counterpartyId":2,"instrumentId":10,"type":"S","amount":9993.35,"quantity":200,"time":"2018-07-30T06:04:22.092","instrumentName":"Eclipse","counterpartyName":"Richard","counterpartyStatus":"A","counterpartyDateRegistered":"2003-01-01T00:00:00.000"},{"id":20825,"counterpartyId":2,"instrumentId":10,"type":"B","amount":10358.57,"quantity":194,"time":"2018-07-30T06:05:41.141","instrumentName":"Eclipse","counterpartyName":"Richard","counterpartyStatus":"A","counterpartyDateRegistered":"2003-01-01T00:00:00.000"},{"id":20858,"counterpartyId":2,"instrumentId":10,"type":"B","amount":10537.86,"quantity":18,"time":"2018-07-30T06:05:41.598","instrumentName":"Eclipse","counterpartyName":"Richard","counterpartyStatus":"A","counterpartyDateRegistered":"2003-01-01T00:00:00.000"},{"id":20861,"counterpartyId":2,"instrumentId":10,"type":"S","amount":10434.21,"quantity":26,"time":"2018-07-30T06:05:41.617","instrumentName":"Eclipse","counterpartyName":"Richard","counterpartyStatus":"A","counterpartyDateRegistered":"2003-01-01T00:00:00.000"},{"id":20958,"counterpartyId":2,"instrumentId":10,"type":"B","amount":10404.71,"quantity":155,"time":"2018-07-30T06:06:02.043","instrumentName":"Eclipse","counterpartyName":"Richard","counterpartyStatus":"A","counterpartyDateRegistered":"2003-01-01T00:00:00.000"}];
		var counterparties_old = [{"id":1,"name":"Lina","status":"A","dateRegistered":"2014-01-01T00:00:00.000","count":379},{"id":2,"name":"Richard","status":"A","dateRegistered":"2003-01-01T00:00:00.000","count":406},{"id":3,"name":"Selvyn","status":"A","dateRegistered":"2016-01-01T00:00:00.000","count":387},{"id":4,"name":"Lewis","status":"A","dateRegistered":"2009-01-01T00:00:00.000","count":406},{"id":5,"name":"Estelle","status":"A","dateRegistered":"2001-01-01T00:00:00.000","count":422}];
		var instruments_old = [{"id":1,"name":"Astronomica","count":167},{"id":2,"name":"Deuteronic","count":176},{"id":3,"name":"Floral","count":170},{"id":4,"name":"Galactia","count":174},{"id":5,"name":"Celestial","count":147},{"id":6,"name":"Heliosphere","count":139},{"id":7,"name":"Jupiter","count":163},{"id":8,"name":"Interstella","count":155},{"id":9,"name":"Koronis","count":187},{"id":10,"name":"Eclipse","count":168},{"id":11,"name":"Borealis","count":181},{"id":12,"name":"Lunatic","count":173}];
		*/

		var deals, counterparties_old, instruments_old;

		$.when(
			$.getJSON("api/deals?with=instrument,counterparty", function(data) {
				deals = data;
			}),
			$.getJSON("api/counterparties?with=count", function(data) {
				counterparties_old = data;
			}),
			$.getJSON("api/instruments?with=count", function(data) {
				instruments_old = data;
			}),
		).then(function() {
			counterparties = {};
			instruments = {};

			counterparties_old.forEach(function(counterparty) {
				counterparties[counterparty.id] = counterparty;
			});

			instruments_old.forEach(function(instrument) {
				instruments[instrument.id] = instrument;
			});


			table.empty();
			instruments_sidebar.empty();
			counterparties_sidebar.empty();

			deals.forEach(function populateDealTable(deal) {
				var row = $("<tr></tr>");
				row.data("deal", deal);
				row.append("<td>" + deal.id + "</td>");
				row.append("<td>" + deal.type + "</td>");
				row.append("<td>" + deal.time + "</td>");
				row.append("<td>" + deal.amount + "</td>");
				row.append("<td>" + (deal.amount * deal.quantity).toFixed(2) + "</td>");
				row.append("<td>" + deal.quantity + "</td>");
				row.append("<td><a href='#' class='tooltip-hint view-instrument-link'>" + deal.instrumentName + "</a></td>");
				row.append("<td><a href='#' class='tooltip-hint view-counterparty-link'>" + deal.counterpartyName + "</a></td>");
				row.appendTo(table);
			});

			$("#loader").fadeOut("fast");
		});

	}
});