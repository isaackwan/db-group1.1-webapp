<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Data Visualisation Application</title>
	<link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="lib/css/login.css">
</head>
<body>
	<div id="ajax-container">
		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="lib/css/main.css">
		<div class="limiter">
			<div class="containerBackground">
				<div id="loginContainer" class="wrap-login100">
					<div class="login100-pic">
						<img id="dblogo" src="lib/images/db.png" alt="IMG">
					</div>
					<form id="loginForm" class="login100-form validate-form" method="POST" action="j_security_check">
						<span class="login100-form-title">
						Member Login
						</span>
						<div class="wrap-input100 validate-input" data-validate="User Name is required">
							<input class="input100" type="text" name="j_username" placeholder="User Name">
							<span class="focus-input100"></span>
							<span class="symbol-input100">
							<i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
							</span>
						</div>
						<div class="wrap-input100 validate-input" data-validate="Password is required">
							<input class="input100" type="password" name="j_password" placeholder="Password">
							<span class="focus-input100"></span>
							<span class="symbol-input100">
							<i class="glyphicon glyphicon-lock" aria-hidden="true"></i>
							</span>
						</div>
						<div id="loginBtn" class="container-login100-form-btn">
							<button class="login100-form-btn" type="submit">
							Login
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- Main Javascript-->
		<script src="lib/bootstrap/js/jquery-3.3.1.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap.min.js"></script>
		<script src="lib/js/login.js"></script>
	</div>
</body>
</html>